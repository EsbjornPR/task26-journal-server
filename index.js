const express = require('express');
// ska cors vara här eller i routes.js eller i båda?
const cors = require('cors');
const app = express();

// Body parser middleware. The third line is to handle html encoded data, i.e. formsubmissions
// or nested form data.
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// The following two lines are to be able to separate the database APIs (or routers?) to separate files. 
// the file will be named for example "router.get" instead of "app.get"
const router = require ('./routes');
app.use(router);

// Destructure the port from the environment variable if not found make it 3000
const { PORT = 3000 } = process.env;





// Testar lite 
app.get('/', (req, res) => {
    return res.status(200).send('Welcome to task 26 server');
});

// Open up a server with a port to listen to
app.listen(PORT, () => console.log(`Server has started on port ${PORT}...`)); 


