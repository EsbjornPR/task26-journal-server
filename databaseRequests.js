// Renamed from app.js
const { Sequelize, QueryTypes } = require('sequelize');

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

const sequelize = new Sequelize({
    database: process.env.DB_DATABASE,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT
});


//************************ Journal database requests *****************************
// Create a journal
async function createJournal(newJournal) {
    try {
        await sequelize.authenticate();
        console.log('Connection established...');
        const [newIndex, nrUpdated] = await sequelize.query(`INSERT INTO journal (name) VALUES ('${newJournal.name}')`, {
            type: QueryTypes.INSERT
        });
        const journal = await sequelize.query(`SELECT * FROM journal WHERE journal_id=${newIndex}`, {
            type: QueryTypes.SELECT
        });
        return journal[0];
    } catch (e) {
        console.error(e);
    }
}

// Uppdating a journal
async function updateJournal(id, updatedJournal) {
    try {
        await sequelize.authenticate();
        console.log('Connection established...');
        // const [ whatIsThis, nrUpdatedFields ] = 
        await sequelize.query(`UPDATE journal SET name = '${updatedJournal.name}' WHERE journal_id=${id}`, {
            type: QueryTypes.UPDATE
        });
        const journal = await sequelize.query(`SELECT * FROM journal WHERE journal_id=${id}`, {
            type: QueryTypes.SELECT
        });
        return journal[0];
    } catch (e) {
        console.error(e);
    }
}

// Getting all entries from a journal
async function getJournalEntries(id) {
    try {
        await sequelize.authenticate();
        console.log('Connection established...');
        const entries = await sequelize.query(`SELECT * FROM journal_entry WHERE journal_id=${id}`, {
            type: QueryTypes.SELECT
        });
        return entries;
    } catch (e) {
        console.error(e);
    }

}

//************************ Entry database requests *****************************
// Getting all tags associated with a specific entry
async function getEntryTags(id) {
    try {
        await sequelize.authenticate();
        console.log('Connection established...');
        const tags = await sequelize.query(`SELECT tag.tag_id, tag.name, tag.created_at, tag.updated_at FROM tag LEFT JOIN journal_entry_tag ON tag.tag_id = journal_entry_tag.tag_id WHERE journal_entry_tag.journal_entry_id = ${id}`, {
            type: QueryTypes.SELECT
        });
        return tags;
    } catch (e) {
        console.error(e);
    }
}

// Create an entry
async function createEntry(newEntry) {
    console.log('Från db funk');
    console.log(newEntry);
    console.log(newEntry.title);
    try {
        await sequelize.authenticate();
        console.log('Connection established...');
        const insertResponse = await sequelize.query(`INSERT INTO journal_entry (title, content, journal_id, author_id) VALUES ('${newEntry.title}', '${newEntry.content}', '${newEntry.journal_id}', '${newEntry.author_id}')`, {
            type: QueryTypes.INSERT
        });
        const entry = await sequelize.query(`SELECT * FROM journal_entry WHERE journal_entry_id=${insertResponse[0]}`, {
            type: QueryTypes.SELECT
        });
        return entry[0];
    } catch (e) {
        console.error(e);
    }
}

// Uppdating an entry - works but sends response before updating database 
// - Leaving it for now have a generell function used by author also with some issues. Make that one work and remove this one
async function updateEntry(id, updatedEntry) {
    try {
        await sequelize.authenticate();
        console.log('Connection established...');
        await Object.entries(updatedEntry).forEach((entry) => {
            console.log(entry);
            sequelize.query(`UPDATE journal_entry SET ${entry[0]} = '${entry[1]}' WHERE journal_entry_id=${id}`, {
                type: QueryTypes.UPDATE
            });
        });
        const journal_entry = await sequelize.query(`SELECT * FROM journal_entry WHERE journal_entry_id=${id}`, {
            type: QueryTypes.SELECT
        });
        return journal_entry[0];
    } catch (e) {
        console.error(e);
    }
}

//************************ Author database requests *****************************
// Adding a new author
async function createAuthor(newAuthor) {
    try {
        await sequelize.authenticate();
        console.log('Connection established...');
        const insertResponse = await sequelize.query(`INSERT INTO author (first_name, last_name, email) VALUES ('${newAuthor.first_name}', '${newAuthor.last_name}', '${newAuthor.email}')`, {
            type: QueryTypes.INSERT
        });
        const author = await sequelize.query(`SELECT * FROM author WHERE author_id=${insertResponse[0]}`, {
            type: QueryTypes.SELECT
        });
        return author[0];
    } catch (e) {
        console.error(e);
    }
}

//************************ General database requests *****************************
// Getting all instances from a database
async function getAll(database) {
    try {
        await sequelize.authenticate();
        console.log('Connection established...');
        const allInstances = await sequelize.query(`SELECT * FROM ${database}`, {
            type: QueryTypes.SELECT
        });
        return allInstances;
    } catch (e) {
        console.error(e);
    }
}

// Getting a specific instance from a database
async function getInstance(database, idLabel,  id) {
    try {
        await sequelize.authenticate();
        console.log('Connection established...');
        const instance = await sequelize.query(`SELECT * FROM ${database} WHERE ${idLabel}=${id}`, {
            type: QueryTypes.SELECT
        });
        return instance[0];
    } catch (e) {
        console.error(e);
    }
}

// Uppdating a database instance
async function updateInstance(database, idLabel, id, updatedInstance) {
    try {
        await sequelize.authenticate();
        console.log('Connection established...');
        await Object.entries(updatedInstance).forEach((entry) => {
            console.log(entry);
            sequelize.query(`UPDATE ${database} SET ${entry[0]} = '${entry[1]}' WHERE ${idLabel}=${id}`, {
                type: QueryTypes.UPDATE
            });
        });
        await setTimeout(() => {
            console.log('Waiting');
        }); 2000;
    } catch (e) {
        console.error(e);
    }
}

// Deleting a database instance
async function deleteInstance(database, idLabel, id ) {
    try {
        await sequelize.authenticate();
        console.log('Connection established...');
        const answer = await sequelize.query(`DELETE FROM ${database} WHERE ${idLabel}=${id}`, {
            type: QueryTypes.DELETE
        });
        console.log(answer);
        return answer;
    } catch (e) {
        console.error(e);
    }
}


module.exports.createJournal = createJournal;
module.exports.updateJournal = updateJournal;
module.exports.getJournalEntries = getJournalEntries;
module.exports.getEntryTags = getEntryTags;
module.exports.createEntry = createEntry;
module.exports.updateEntry = updateEntry;
module.exports.getAll = getAll;
module.exports.getInstance = getInstance;
module.exports.updateInstance = updateInstance;
module.exports.createAuthor = createAuthor;
module.exports.deleteInstance = deleteInstance;