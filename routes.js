const express = require('express');
const router = express.Router();
const db = require('./databaseRequests');

// req is the request from the client and res is the respont we send back to the client

//************************ Journal routes *****************************
// Getting all journals
router.get('/v1/api/journals', (req, res) => {
    db.getAll('journal').then(response => {
        return res.status(200).json(response);
    });
});

// Getting a specific journal
router.get('/v1/api/journals/:id', (req, res) => {
    const id = req.params.id;
    db.getInstance('journal', 'journal_id', id).then(response => {
        return res.status(200).json(response);
    });
});

// Creating a new journal
router.post('/v1/api/journals', (req, res) => {
    db.createJournal(req.body).then(response => {
        return res.status(201).json(response);
    });
});

// Updating a journal
router.patch('/v1/api/journals/:id', (req, res) => {
    const id = req.params.id;
    db.updateJournal(id, req.body).then(response => {
        return res.status(200).json(response);
    });
});

// Getting all entries from a journal
router.get('/v1/api/journals/:id/entries', (req, res) => {
    const id = req.params.id;
    db.getJournalEntries(id).then(response => {
        return res.status(200).json(response);
    });
});

// Deleting a journal
router.delete('/v1/api/journals/:id', (req, res) => {
    const id = req.params.id;
    if (id >= 1) {
        db.deleteInstance('journal', 'journal_id', id).then(response => {
            return res.status(200).json(response);
        });
    } else {
        return res.status(400).json('Not a valid request');
    }
});


//************************ Entry routes *****************************
// Getting all entries
router.get('/v1/api/entries', (req, res) => {
    db.getAll('journal_entry').then(response => {
        return res.status(200).json(response);
    });
});

// Getting a specific entry
router.get('/v1/api/entries/:id', (req, res) => {
    const id = req.params.id;
    db.getInstance('journal_entry', 'journal_entry_id', id).then(response => {
        return res.status(200).json(response);
    });
});

// Getting all tags associated with a specific entry
router.get('/v1/api/entries/:id/tags', (req, res) => {
    const id = req.params.id;
    db.getEntryTags(id).then(response => {
        return res.status(200).json(response);
    });
});

// Creating a new entry
router.post('/v1/api/entries', (req, res) => {
    db.createEntry(req.body).then(response => {
        return res.status(201).json(response);
    });
});

// Updating an entry
router.patch('/v1/api/entries/:id', (req, res) => {
    const id = req.params.id;
    db.updateEntry(id, req.body).then(response => {
        return res.status(200).json(response);
    });
});

// Deleting an entry
router.delete('/v1/api/entries/:id', (req, res) => {
    const id = req.params.id;
    if (id >= 1) {
        db.deleteInstance('journal_entry', 'journal_id', id).then(response => {
            return res.status(200).json(response);
        });
    } else {
        return res.status(400).json('Not a valid request');
    }
});

//************************ Author routes *****************************
// Getting all authors
router.get('/v1/api/authors', (req, res) => {
    db.getAll('author').then(response => {
        return res.status(200).json(response);
    });
});

// Getting a specific author
router.get('/v1/api/authors/:id', (req, res) => {
    const id = req.params.id;
    db.getInstance('author', 'author_id', id).then(response => {
        return res.status(200).json(response);
    });
});

// Adding a new author
router.post('/v1/api/authors', (req, res) => {
    db.createAuthor(req.body).then(response => {
        return res.status(201).json(response);
    });
});

// Updating an author
// Leaving it for now
// Still has some issues related to responding before database is uppdated
// A filter needs to be added on this side for filtering out fields that are allowed to updatae
router.patch('/v1/api/authors/:id', (req, res) => {
    const id = req.params.id;
    db.updateInstance('author', 'author_id', id, req.body).then(response => {
        db.getInstance('author', 'author_id', id).then(response => {
            return res.status(200).json(response);
        });
    });
});

// Deleting an author
router.delete('/v1/api/authors/:id', (req, res) => {
    const id = req.params.id;
    if (id >= 1) {
        db.deleteInstance('author', 'author_id', id).then(response => {
            return res.status(200).json(response);
        });
    } else {
        return res.status(400).json('Not a valid request');
    }
});

//************************ Tag routes *****************************
// Getting all tags
router.get('/v1/api/tags', (req, res) => {
    db.getAll('tag').then(response => {
        return res.status(200).json(response);
    });
});

// Getting a specific tag
router.get('/v1/api/tags/:id', (req, res) => {
    const id = req.params.id;
    db.getInstance('tag', 'tag_id', id).then(response => {
        return res.status(200).json(response);
    });
});

module.exports = router;