## Task 26 - API server side
This is the server part of task 26 using Node, Express and Sequelize ORM
in order to provide the client part with APIs to access the journal database.